
pub fn format_cli_duration(to_format: chrono::Duration ) -> String
{
    let _seconds = to_format.num_seconds() % 60;
    let minutes = (to_format.num_seconds() / 60)% 60;
    let hours = (to_format.num_seconds() / 60) / 60;

    format!("{:02}h {:02}m", hours, minutes)
}