use chrono::{Duration, NaiveDateTime, DurationRound};
use std::collections::BTreeMap;
use std::fs::{read_to_string, OpenOptions};
use std::io::prelude::*;
use std::option::Option;
use std::path::PathBuf;
use wildmatch::WildMatch;

#[cfg(test)]
mod tests;

pub struct TimelogParseResult {
    pub entries: BTreeMap<NaiveDateTime, (String, Vec<String>, Duration)>,
    pub worktime: Duration,
    pub breaktime: Duration,
}
pub struct TimelogParser {
    path: PathBuf,
}

impl TimelogParser {

    pub fn new(path: PathBuf) -> TimelogParser {
        let object = TimelogParser { path: path };
        object.create_dir_and_file_when_necessary();
        object
    }

    fn create_dir_and_file_when_necessary(&self) {
        if !self.path.exists()
        {
            let basepath = self.path.parent().unwrap();

            match std::fs::create_dir_all(basepath) {
                Ok(_result) => {}
                Err(error) => {panic!("Unable to create task base path: {}", error)}
            }

            match std::fs::File::create(&self.path) {
                Ok(_result) => {}
                Err(error) => {panic!("Unable to create task file: {}", error)}
            }
        }
    }

    pub fn append_entry_to_file(&self, date: NaiveDateTime, message: String) -> Result<(), String>{
        let path = &self.path;

        if self.check_is_date_already_used(date) {
            return Err("Date already has an entry. Not appended.".to_string())
        }

        let mut output = OpenOptions::new()
            .write(true)
            .append(true)
            .create(true)
            .open(path)
            .unwrap();

        if let Err(e) = writeln!(output, "{}: {}", date.format("%Y-%m-%d %H:%M"), message) {
            return Err(format!("Couldn't write to file: {}",  e));
        }
        Ok(())
    }
    /// Checks if the date is already in the log
    /// 'date' Date to check
    fn check_is_date_already_used(&self, date: NaiveDateTime) -> bool{
        let date_trunc = date.duration_trunc(Duration::milliseconds(60*1000)).unwrap();
        let result = self.get_range(date_trunc, date_trunc, None);
        !result.entries.is_empty()
    }

    pub fn get_range(
        &self,
        from: NaiveDateTime,
        to: NaiveDateTime,
        filter: Option<String>,
    ) -> TimelogParseResult {
        let mut result: BTreeMap<NaiveDateTime, (String, Vec<String>, Duration)> = BTreeMap::new();
        let path = &self.path;

        let mut former_date: Option<NaiveDateTime> = None;
        for line in read_to_string(path).unwrap().lines() {
            if !line.starts_with("#") && !line.is_empty() {
                let (date, description, tags, duration) =
                    match TimelogParser::convert_line(line, former_date) {
                        Ok(result) => result,
                        Err(message) => {
                            println!("{}", message);
                            continue;
                        },
                    };
                former_date = Some(date);
                if (date >= from)
                    && (date <= to)
                    && (match filter {
                        Some(ref x) => WildMatch::new(x).matches(&description),
                        None => true,
                    })
                {
                    result.insert(date, (description, tags, duration));
                }
            }
        }

        //Calculate worktime
        let mut breaktime: Duration = Duration::seconds(0);
        let mut worktime: Duration = Duration::seconds(0);
        for (_date, (description, _tags, duration)) in &result {
            if description.ends_with("arrived**") {
                //ignore is arrived marker
            } else if description.ends_with("**") {
                breaktime = breaktime + *duration;
            } else {
                worktime = worktime + *duration;
            }
        }

        let retval: TimelogParseResult = TimelogParseResult {
            entries: result,
            worktime: worktime,
            breaktime: breaktime,
        };

        retval
    }

    fn convert_line(
        line: &str,
        former_date: Option<NaiveDateTime>,
    ) -> std::result::Result<(NaiveDateTime, String, Vec<String>, Duration), &'static str> {
        let mut splitted_line = line.splitn(3, ':');
        let mut date = String::new();

        date.push_str(
            match splitted_line.next() {
                Some(first_date_part) => first_date_part,
                None => return Err("Error parsing first part date")
            });
        date.push(':');
        date.push_str(
            match splitted_line.next() {
                Some(second_date_part) => second_date_part,
                None => return Err("Error parsing second part date")
            });

        let description_with_tags: String = match splitted_line.next() {
            Some(description_and_tags) => description_and_tags.to_string(),
            None => "".to_string()
        };

        let mut splitted_description_tags = description_with_tags.split("--");

        let description: String = match splitted_description_tags.next() {
            Some(description) => description.trim().to_string(),
            None => "".to_string()
        };

        let tags = match splitted_description_tags.next() {
            Some(tag_strings) => {
                let mut tags: Vec<String> = Vec::new();
                for tag in tag_strings.split(" ") {
                    let trimmed_tag = tag.trim().to_string();
                    if !trimmed_tag.is_empty() {
                        tags.push(trimmed_tag.to_string());
                    }
                }
                tags
            }
            None => {
                let tags: Vec<String> = Vec::new();
                tags
            }
        };

        let date = match NaiveDateTime::parse_from_str(&date, "%Y-%m-%d %H:%M")
        {
            Ok(date) => date,
            _ => return Err("Unable to parse date")
        };

        let duration = match former_date {
            Some(former_date) => {
                if description == "arrived**" {
                    Duration::days(0)
                } else {
                    date - former_date
                }
            }
            None => Duration::days(0),
        };
        Ok((date, description, tags, duration))
    }
}

pub struct TasksParser {
    path: PathBuf,
}

impl TasksParser {
    pub fn new(path: PathBuf) -> TasksParser {
        let object = TasksParser { path: path };
        object.create_dir_and_file_when_necessary();
        object
    }

    fn create_dir_and_file_when_necessary(&self) {
        if !self.path.exists()
        {
            let basepath = self.path.parent().unwrap();

            match std::fs::create_dir_all(basepath) {
                Ok(_result) => {}
                Err(error) => {panic!("Unable to create task base path: {}", error)}
            }

            match std::fs::File::create(&self.path) {
                Ok(_result) => {}
                Err(error) => {panic!("Unable to create task file: {}", error)}
            }
        }
    }

    pub fn get_tasks(&self) -> BTreeMap<i32, String> {
        let path = &self.path;

        let mut tasks: BTreeMap<i32, String> = BTreeMap::new();
        let mut i = 0;
        for line in read_to_string(path).unwrap().lines() {
            if !line.starts_with("#") {
                tasks.insert(i, line.to_string());
                i += 1;
            }
        }

        tasks
    }
}