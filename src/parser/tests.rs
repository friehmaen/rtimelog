
use std::{path::{PathBuf, Path}, str::FromStr};

use super::*;

//TODO: Auto call these after each test
fn test_cleanup(testfolder: &Path)
{
    match std::fs::remove_dir_all(testfolder) {
        Ok(_) => (),
        Err(_) => panic!("Error during cleanup")
    };
}

fn generate_random_test_file() -> PathBuf
{
    let mut random_file = String::from("./");
    random_file.push_str(&random_string::generate(20, random_string::charsets::NUMERIC));
    random_file.push_str("/rtimelog.txt");
    PathBuf::from(random_file)
}

#[test]
fn appends_message_to_file() {
    let testfile = generate_random_test_file();
    let test_string = "Test string".to_string();
    let test_date= NaiveDateTime::parse_from_str("2015-09-05 23:56:04", "%Y-%m-%d %H:%M:%S").unwrap();
    let expected_content = test_date.format("%Y-%m-%d %H:%M").to_string() + ": " + &test_string;

    let ut = TimelogParser::new(testfile.clone());
    assert!(!ut.append_entry_to_file(test_date, test_string).is_err());


    let content = std::fs::read_to_string(testfile.clone())
    .expect("Should have been able to read the file");

    assert_eq!(content.trim(), expected_content.trim());

    //TODO: How to always execute this?
    test_cleanup(testfile.parent().unwrap());
}

#[test]
fn does_not_append_same_date_to_file() {
    let testfile = PathBuf::from("./testfolder/testfile.txt");
    let test_string = "Test string".to_string();
    let test_date= NaiveDateTime::parse_from_str("2023-12-24 22:00:04", "%Y-%m-%d %H:%M:%S").unwrap();
    let expected_content = test_date.format("%Y-%m-%d %H:%M").to_string() + ": " + &test_string;

    let ut = TimelogParser::new(testfile.clone());
    assert!(!ut.append_entry_to_file(test_date, test_string.clone()).is_err());
    assert!(ut.append_entry_to_file(test_date, test_string).is_err());


    let content = std::fs::read_to_string(testfile.clone())
    .expect("Should have been able to read the file");

    assert_eq!(content.trim(), expected_content.trim());

    //TODO: How to always execute this?
    test_cleanup(testfile.parent().unwrap());
}